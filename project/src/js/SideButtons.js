var SideButtons = {
    initialise: function () {
        SideButtons.events.initialise();
    },

    pausedOnOpen: false,

    /**
     * Define the side buttons
     */
    buttons: {
        button1: function() {
            if (SideButtons.isCardOpen('button1')) {
                SideButtons.closeSideCard('button1', true);
            } else {
                SideButtons.openSideCard('button1');
            }
        },

        button2: function() {
            if (SideButtons.isCardOpen('button2')) {
                SideButtons.closeSideCard('button2', true);
            } else {
                SideButtons.openSideCard('button2');
            }
        },

        button3: function() {
            if (SideButtons.isCardOpen('button3')) {
                SideButtons.closeSideCard('button3', true);
            } else {
                SideButtons.openSideCard('button3');
            }
        },

        button4: function() {
            if (SideButtons.isCardOpen('button4')) {
                SideButtons.closeSideCard('button4', true);
            } else {
                SideButtons.openSideCard('button4');
            }
        },

        button5: function() {
            VideoPlayerInterface.actions.pause();
            // Log the click event
            VideoPlayerInterface.iframeWindow.rtc.utils.track("sidebutton.click", "button5");
            window.open("http://www.pitneybowes.com/us/customer-engagement-marketing/synchronized-communications-execution/engageone-video-personalized-video.html", '_blank');
        }
    },

    /**
     * Define the interaction cards that are "side cards"
     */
    cardMap: {
        button1: 'ade94bd1',
        button2: '4d71f0ff',
        button3: '14c94b8c',
        button4: 'ade94bd1'
    },

    /**
     * Display the side card, closing any open interaction cards or pausing the video
     */
    openSideCard: function(card) {
        SideButtons.closeAllSideCards();
        VideoPlayerInterface.hideResumeSplash();

        // Log the card open event
        VideoPlayerInterface.iframeWindow.rtc.utils.track("sidebutton.click", card);

        // Hide any interaction cards in the video and show the side card overlay instead
        //VideoPlayerInterface.iframeWindow.closeAllOtherCards();

        // The function above should be defined in your project's common javascript
        // in the Interactions Manager in the EOV Director.
        // Sample code is shown below:

        // window.cardsToReopen = [];
        // window.closeAllOtherCards = function() {
        //     $('.card').each(function(e) {
        //         if ($(this).is(":visible")) {
        //             window.cardsToReopen.push($(this));
        //             $(this).rtcCard("close")
        //         }
        //     });
        // };
        // window.reopenCards = function() {
        //     for (var i = 0; i < window.cardsToReopen.length; i++) {
        //         window.cardsToReopen[i].rtcCard("open");
        //     }
        //     window.cardsToReopen = [];
        // };

        SideButtons.pausedOnOpen = !VideoPlayerInterface.isPlaying;
        VideoPlayerInterface.actions.pause(false);

        VideoPlayerInterface.iframeWindow.$("#card" + SideButtons.cardMap[card]).rtcCard("open");
    },

    /**
     * Close the side card, reopening any previously opened interaction cards or resuming the video
     */
    closeSideCard: function(card, continueVideo) {
        VideoPlayerInterface.iframeWindow.$("#card" + SideButtons.cardMap[card]).rtcCard("close");
        // Re-open any interaction cards that were previously open
        //VideoPlayerInterface.iframeWindow.reopenCards();

        // See code snippet and comments in "openSideCard" function above
        // for examples of how to use this code

        if (continueVideo && !SideButtons.pausedOnOpen) {
            VideoPlayerInterface.actions.play();
        }
    }, 

    /**
     * Close all open side cards
     */
    closeAllSideCards: function() {
        for (var card in SideButtons.cardMap) {
            SideButtons.closeSideCard(card);
        }
    },

    /**
     * Find out if a particular card is currently "open"
     */
    isCardOpen: function(card) {
        return VideoPlayerInterface.iframeWindow.$("#card" + SideButtons.cardMap[card] + ":visible").length > 0;
    },

    /**
     * Define the events for the side buttons
     */
    events: {
        initialise: function () {
            $('.side-button-container button').click(SideButtons.events.click);
        },

        click: function(e) {
            SideButtons.buttons[$(this).data('button')]();
        }
    }
};
