$(document).ready(function() {

    var containerBoxMargin = 0;
    var desiredMargin = 0;

    /**
     * Update the viewport meta tag so the device scales correctly on mobile devices
     */
    function updateViewport() {
        document.getElementById('vp').setAttribute(
            'content',
            screen.width < 768 ? 'width=767' : 'width=device-width, initial-scale=1'
        );

        var currentMargin = parseInt($(".container-box").css("margin-top"));

        if (desiredMargin == 0) {
            // If the top margin has not yet been set, set it from the current margin.
            desiredMargin = currentMargin;
        }

        // Remove the top margin if the window is too short
        if ($(window).height() < ($(".container-box").height() + desiredMargin)) {
            if (currentMargin !== 0) {
                containerBoxMargin = currentMargin
            }

            $(".container-box").css("margin-top", 0);
        } else {
            if (containerBoxMargin != 0) {
                $(".container-box").css("margin-top", containerBoxMargin);
            }
        }
    }

    updateViewport();

    $(window).resize(function() {
        updateViewport();
    });

    /**
     * Initialise timeline
     */
    Timeline.initialise(0);

    /**
     * Initialise contrast progress bar
     */
    ContrastProgress.initialise();

    /**
     * Initialise side buttons
     */
    SideButtons.initialise();

    /**
     * Initialise promos
     */
    Promos.initialise();

    /**
     * Initialise volume slider
     */
    VolumeSlider.initialise();

    /**
     * Initialise language selector
     */
    LanguageSelector.initialise();

    /**
     * Initialise closed caption selector
     */
    ClosedCaptionSelector.initialise();

    /**
     * Initialise video player interface
     */
    VideoPlayerInterface.initialise();
    // Quality selector is initialised in VideoPlayerInterface.updateFromVideo().

    KeyboardInputController.initialise();
});